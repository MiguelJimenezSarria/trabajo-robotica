delete(instrfind({'Port'},{'COM8'}));
pserial=serial('COM8','BaudRate',9600);
fopen(pserial);
syms v1 v2 v3 v4 v5
v1=0;
v2=0;
v3=0;
v4=0;
v5=0;
% Tita, d, a, alfa, TIPO
L(1)=Link([0 30 0 -pi/2 0]);
L(2)=Link([-pi/2 -2 34.5 0 0]); 
L(3)=Link([0 7 17 0 0]);
L(4)=Link([-pi/2 0 0 -pi/2 0]);
L(5)=Link([0 6 0 0 0]); 
RTrabajo = SerialLink(L);
RTrabajo.plot([0 0 0 0 0],'workspace',[-100 200 -100 200 -100 200]);
RTrabajo.teach();
contador=0;
while 1    
    if contador<5
     entrada=fscanf(pserial)
[ValoresPrueba,matches] = strsplit(entrada,{'&',','},'CollapseDelimiters',true);
if  matches(1)=="&"    
  ValoresCinematicaString=ValoresPrueba(2:6);
ValoresCinematica=str2double(ValoresCinematicaString);
end
v1=ValoresCinematica(1)*2*pi/1024;
v2=ValoresCinematica(2)*2*pi/1024;
v3=ValoresCinematica(3)*2*pi/1024;
v4=ValoresCinematica(4)*2*pi/1024;
v5=ValoresCinematica(5)*2*pi/1024;
q=[v4 v2 v3 v1 v5]
    else
    RTrabajo.plot(q);
    contador=0;
    end
 
contador=contador+1;
end
fclose(pserial); 
delete(pserial);
clear all;