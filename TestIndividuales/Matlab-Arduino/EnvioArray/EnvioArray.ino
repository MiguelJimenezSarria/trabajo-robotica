

String PT; //VALORES STRING ENVIADA POR MATLAB EN BRUTO
String Valores; //VALORES STRING ENVIADA POR MATLAB CON RECONOCIMIENTO DE & (BIT DE INICIO) ??
String var_00,var_01,var_02,var_03; //STRINGS DE CADA ELEMENTO DE LA STRING ENVIADA, DIVIDAS POR ','
int var[4]; // VALORES INT DE CADA STRING  //VALORES A USAR


//DEMO
#define  LED1 2
#define LED2 3
#define LED3 4
#define LED4 6

 void setup(){
Serial.begin(115200); //Mismo valor que en matlab
Serial.println("Ready");  //Comprobar comunicación matlab-arduino


//DEMO
pinMode(LED1,OUTPUT);
pinMode(LED2,OUTPUT);
pinMode(LED3,OUTPUT);
pinMode(LED4,OUTPUT);
   
}

 void loop() {
  
if (Serial.available() > 0) { 
 PT=Serial.readStringUntil('\n'); //LEE STRING
 delay(500); 
}

//ESTO SE DEBERÑIA DE HACER TODO EN UNA FUNCIÓN SUPONGO :D

Valores=getValue(PT,'&',1); //SI LA LECTURA DE MATLAB NO EMPIEZA POR & NO LA TRATA
//SEPARACIÓN DE STRINGS
var_00=getValue(Valores,',',0);    
var_01=getValue(Valores,',',1);   
var_02=getValue(Valores,',',2);  
var_03=getValue(Valores,',',3); 
//DE STRING A INT
var[0]=var_00.toInt();
var[1]=var_01.toInt();
var[2]=var_02.toInt();
var[3]=var_03.toInt();


  delay(500);
  //DEMO
  digitalWrite(LED1, var[0]);
  digitalWrite(LED2, var[1]);
 digitalWrite(LED3, var[2]);
  digitalWrite(LED4, var[3]);

}


 String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
   return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
