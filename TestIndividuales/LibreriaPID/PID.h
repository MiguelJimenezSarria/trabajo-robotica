

class PID_R{
  public:
  float PID_error, PID_D_error, PID_I_error, PID_erroranterior;
  float tiempoprograma, tiempoactual, tiempoanterior;
  
  void Init(float &Input, float &Output, float &Consigna, float Kp, float Ki, float Kd){
    
   tiempoanterior=tiempoactual; //Para calcular el tiempo de ejecucion del programa
   tiempoactual = millis();  //Nos da el tiempo que lleva encendido el arduino
   tiempoprograma = (tiempoactual - tiempoanterior)/1000; //El tiempo de ejecución del programa

   PID_error = Consigna - Input ; //Calculamos el error
   
   PID_I_error += Ki*PID_error*tiempoprograma;

   if(PID_I_error>30){
    PID_I_error=30;
   }
   if(-2<PID_error && PID_error<2){
     PID_I_error=0;
   }

   PID_D_error = Kd*(PID_error - PID_erroranterior)/tiempoprograma;

   Output= Kp*(PID_error + PID_I_error + PID_D_error);
  
   PID_erroranterior= PID_error;
}
  
};
