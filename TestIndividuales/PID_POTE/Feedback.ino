#define pot1  A1
#define pot2  A2
#define pot3  A3
#define pot4  A4
#define pot5  A5
#define pot6  A6

const int nDOF=6;
int PotPin[6]={pot1,pot2,pot3,pot4,pot5,pot6};
int feedback[nDOF]; 
   
void feedback_init(){
 for(int i=0;i<nDOF;i++){
    pinMode(PotPin[i],INPUT);
 }
  
}

void feedback_read(){
   
 for(int i=0;i<nDOF;i++){
       feedback[i] = analogRead(PotPin[i]);          // realizar la lectura analógica raw
 }
}
