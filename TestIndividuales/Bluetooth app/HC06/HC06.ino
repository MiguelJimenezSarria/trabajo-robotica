
char c[10];
//COMANDOS SERIAL 
/*
AT NAME-> TE DICE EL NOMBRE
AT NAME name-> LO LLAMA name
AT -> Si devuelve OK buena conexión
AT+PASS-> Te dice la contraseña
AT RESET-> para meter la configuración dada
 * 
 */
 //leds
int LEDverde=3;
int LEDrojo=2;


#define T 20
float consigna[T][5];
float q0[5]={0, 0, 0, 0, 0};
//float qf[5]={1, 2, 3, 4, 5};


float posicion_inicial[3]; 
float posicion_final[3];  
float posicion_interpolar[3]; 

///
void setup() 
{
    Serial.begin(9600);
    Serial.println("Arduino listo");
    Serial.println("Recuerda seleccionar Ambos NL & CR en el monitor serial");
    pinMode(2,OUTPUT); //Pin para control del Reset del modulo HC05
    digitalWrite(19, LOW);
    delay(200);
    digitalWrite(19, HIGH);
    
    Serial1.begin(9600);  //HC-05 38400
    pinMode(LEDrojo,OUTPUT);
    pinMode(LEDverde,OUTPUT);

    posicion_inicial[0]=0;
    posicion_inicial[1]=0;
    posicion_inicial[2]=0;
}
 
void loop()
{ 
   
    // Esperar respuesta del modulo HC-06 y se envia a el monitor serial
    if (Serial1.available())
    {  
        String str = Serial1.readStringUntil('\n');
        Serial.println(str);
/////////LEDS
        //Serial.println(str.length());
        //Serial.println(str[0]);
        if(str[0]=='1'){  // chocolate
          digitalWrite(LEDrojo,!digitalRead(LEDrojo));

          posicion_final[0]=30;
          posicion_final[1]=30;
          posicion_final[2]=30;
          Serial.println(posicion_inicial[1]);
        } 
        if(str[0]=='2'){  ///nata
          digitalWrite(LEDverde,!digitalRead(LEDverde));

          posicion_final[0]=60;
          posicion_final[1]=60;
          posicion_final[2]=30;
        }


        //interpolar
        Serial.println("Vector para interpolar:");
       for (int i=0;i<3;i++){
          posicion_interpolar[i]=(posicion_final[i]-posicion_inicial[i])/T;
          Serial.print(posicion_interpolar[i]);
          Serial.print(" ");
        } 
        //matriz consigna
        Serial.println("");
        Serial.println("Matriz consigna: ");
       for (int i=0;i<3;i++){              ///las filas de Q
        for (int j=0;j<=T;j++){            /// las columnas
          consigna[i][j]=posicion_inicial[i]+j*posicion_interpolar[i];   /// colummna del Q1 es igual 
          Serial.print(consigna[i][j]);
          Serial.print(" ");
        }
        Serial.println("");
       }
        for (int i=0;i<3;i++){
         posicion_inicial[i] = posicion_final[i];
        } 
    }



}
/*
interpolacion(posicion_interpolar,posicion_inicial,posicion_final);
    ///escribir en los motores
    
matriz_consigna(consigna[][],posicion_interpolar ,posicion_inicial);
    
}

void matriz_consigna(float consigna[][T],float qv[], float q0[]){
  for (int i=0;i<5;i++){              ///las filas de Q
    for (int j=0;j<T;j++){            /// las columnas
      consigna[i][j]=q0[i]+T*qv[i];   /// colummna del Q1 es igual 
    }
  }
}
void interpolacion(float qv[],float q0[],float qf[]){
  
  for (int i=0;i<5;i++){
    qv[i]=(qf[i]-q0[i])/T;
  }

}

struct posicion trayectoria_posicion(struct posicion p1, struct posicion p2){
  posicion res;
  res.x=(p2.x-p1.x)/T;
  res.y=(p2.y-p1.y)/T;
  res.y=(p2.y-p1.y)/T;
  return res;
}
*/
