

///////////////Hay que dividir la funcion INIT en 2

class PID_R{
  public:
  float PID_error, PID_D_error, PID_I_error, PID_erroranterior;
  float tiempoprograma, tiempoactual, tiempoanterior;
  float Kp, Ki, Kd;
  float Max=100, Min=100; //Default
  
  void Init(float P, float I, float D){
    Kp=P;
    Ki=I;
    Kd=D;
  
  }

   void SetOutputLimits(int max, int min){

    Max=max;
    Min=min;
    
  }

  void Calc(float Input, float &Output, float Consigna){
  
    tiempoanterior=tiempoactual; //Para calcular el tiempo de ejecucion del programa
    tiempoactual = millis();  //Nos da el tiempo que lleva encendido el arduino
    tiempoprograma = (tiempoactual - tiempoanterior)/1000; //El tiempo de ejecución del programa

    PID_error = Consigna - Input ; //Calculamos el error
   
    PID_I_error += Ki*PID_error*tiempoprograma;

    if(PID_I_error>30){ //Anti windup
      PID_I_error=30;
    }
    if(-2<PID_error && PID_error<2){
     PID_I_error=0;
    }

    PID_D_error = Kd*(PID_error - PID_erroranterior)/tiempoprograma;

    Output= constrain(Kp*(PID_error + PID_I_error + PID_D_error),-255,255);
  
    PID_erroranterior= PID_error;
  }

 
};
