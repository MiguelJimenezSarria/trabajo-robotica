
class L298N: public HBridge{
  public:
    int enable, L, R;
    
     void Init(int En, int _L, int _R){ //no me acuerdo de como hacer eto en reducido y no lo encuentro
        enable=En;
        L=_L;
        R=_R;

      pinMode(enable,OUTPUT); 
      pinMode(L,OUTPUT); 
      pinMode(R,OUTPUT); 
      
        }
    
    void Enable(){
      
      //analogWrite(enable,255); //Hay que comprobar si explota
      digitalWrite(L, LOW);
      digitalWrite(R, LOW);
      
    }

    void Turn(int pwm){
      if(pwm>0){
        
        digitalWrite(R, HIGH);
        digitalWrite(L, LOW);
        analogWrite(enable,pwm);
      // Serial.print(pwm);
        
      }
    
      if(pwm<0){
        digitalWrite(L, HIGH);
        digitalWrite(R, LOW);
        analogWrite(enable,-pwm);
       
      }
    }

};
