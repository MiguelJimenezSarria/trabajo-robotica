 #include "PID.h"
 #define T 1
 #include <Servo.h>
 Servo servo_lacasitos;
 Servo servo_virutas;
 
 int pin_servo_lacasitos=45; 
 int pin_servo_virutas=46;
 int rele_bomba=40;
 
//trayectorias
float posicion_inicial[5]; 
float posicion_final[5];  
float posicion_interpolar[5]; 

float matriz_consigna[T][5];

//PID
PID_R PID0, PID1, PID2, PID3, PID4, PID5;

const int nDOF=6;
float feedback[nDOF]; 
float Output[nDOF];

float consigna[5];



void setup() {
  
  Serial.begin(115200);

  servo_virutas.attach(pin_servo_virutas);
  servo_virutas.write(135);
  pinMode(rele_bomba, OUTPUT);
  digitalWrite(rele_bomba,HIGH);
  feedback_init();
  PWM_Init();
  
  PID0.Init(1.3,0.8,0);//1.3,0.5,0
  PID1.Init(3,0.7,0);//3,0.6
  PID2.Init(3,0.6,0);//2,0.3
  PID3.Init(2,0.4,0);//2,0.4
  PID4.Init(4,0.5,0);//4,0.5

  consigna[0]=-180;
  consigna[1]=-90;
  consigna[2]=0;
  consigna[3]=-90;
  consigna[4]=0;
  /*
  posicion_inicial[0]=-180;
  posicion_inicial[1]=-90;
  posicion_inicial[2]=0;
  posicion_inicial[3]=-90;
  posicion_inicial[4]=0;
  
  posicion_final[0]=-180;
  posicion_final[1]=-45;
  posicion_final[2]=45;
  posicion_final[3]=0;
  posicion_final[4]=0;*/
 interpolador();
 
}

void loop() {

  
 
    feedback_read();
    PWM_write(); 
    
    feedback[0]=map(feedback[0],0,1300,0,360)-280;
    feedback[1]=map(feedback[1],0,1273,360,0)-260;
    feedback[2]=map(feedback[2],0,1023,360,0)-81;
    feedback[3]=map(feedback[3],0,1200,0,360)-109;
    feedback[4]=map(feedback[4],0,1023,0,360)-180;
    
  sincronizador();
   PID0.Calc(feedback[0], Output[0], consigna[0]); //input, output, consigna
   PID1.Calc(feedback[1], Output[1], consigna[1]); 
   PID2.Calc(feedback[2], Output[2], consigna[2]); 
   PID3.Calc(feedback[3], Output[3], consigna[3]); 
   PID4.Calc(feedback[4], Output[4], consigna[4]);

    
   visualizar();
  
  
}
