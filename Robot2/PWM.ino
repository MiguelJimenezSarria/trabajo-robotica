#include "BTS7960.h"
#include "L298N.h"

//Puente H L298N hay 2

#define R4 14 /////////DCHA Y IZDA SE PUEDEN PONER EN PINES DIGITALES SI HACEN FALTA. LO HE PUESTO PORQUE ESTEN AL LADO DE LOS MOTORES DEL OTRO PUENTE H
#define L4 15
#define EnablePin4 12


//PUENTES H BTS796 hay 4

#define L0_PWM  2    //Necesita un pin para cada lado
#define R0_PWM  3
#define L1_PWM  4
#define R1_PWM  5
#define L2_PWM  6
#define R2_PWM  7
#define L3_PWM  8
#define R3_PWM  9
#define EnablePin0123  0

//Gripper
#define L_Gripper 10
#define R_Gripper 11
#define EnableGripper 51

BTS7960 motor0, motor1, motor2, motor3, gripper; 
L298N motor4, motor5;

void PWM_Init(){
  gripper.Init(EnableGripper, L_Gripper, R_Gripper);
    motor0.Init(EnablePin0123, L0_PWM, R0_PWM);
    motor1.Init(EnablePin0123, L1_PWM, R1_PWM);
    motor2.Init(EnablePin0123, L2_PWM, R2_PWM);
    motor3.Init(EnablePin0123, L3_PWM, R3_PWM);
    
    motor4.Init(EnablePin4,L4,R4);
    gripper.Enable();
     motor0.Enable();
     motor1.Enable();
     motor2.Enable();
     motor3.Enable();
     motor4.Enable();
     
     
}


void PWM_write() {
  
  motor0.Turn(Output[0]); 
  motor1.Turn(Output[1]);
  motor2.Turn(Output[2]);
  motor3.Turn(Output[3]);
  motor4.Turn(Output[4]);


}
void gripper_close(){
  digitalWrite(EnableGripper,HIGH);
  gripper.Turn(+200);
  delay(700);
  gripper.Turn(0);
   digitalWrite(EnableGripper,LOW);
}
void gripper_open(){
   digitalWrite(EnableGripper,HIGH);
 gripper.Turn(-200);
  delay(700);
  gripper.Turn(0);
   digitalWrite(EnableGripper,LOW);
}
