#include "HBridge.h"

class BTS7960: public HBridge{
  public:
  
    int EN;
    int _L_PWM;
    int _R_PWM;
    
  void Init(int EN, int L_PWM, int R_PWM){
    
     _R_PWM = R_PWM;
     _L_PWM = L_PWM;
     EN = EN;
     pinMode(_R_PWM, OUTPUT);
     pinMode(_L_PWM, OUTPUT);
     pinMode(EN, OUTPUT);
  }
    
    
   void Enable(){
    
      digitalWrite(EN,1);
   }
   
   void Disable(){
    
    digitalWrite(EN,0);
   }

   

   void Turn(int pwm){
      if(pwm>=0){
        analogWrite(_R_PWM, 0);
        delayMicroseconds(100);
        analogWrite(_L_PWM, pwm);
        
      }
    
      if(pwm<0){
        analogWrite(_L_PWM, 0);
        delayMicroseconds(100);
        analogWrite(_R_PWM, -pwm);
     
      }
   }

  
   
   void Stop(){

    analogWrite(_L_PWM, LOW);
    analogWrite(_R_PWM, LOW);

   }

 
};
