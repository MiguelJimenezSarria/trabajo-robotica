clear all, clc

%Parametros DH
% Tita, d, a, alfa, TIPO
L(1)=Link([-pi 30 0 -pi/2 0]);
L(2)=Link([-pi/2 -2 34.5 0 0]); 
L(3)=Link([0 7 17 0 0]);
L(4)=Link([-pi/2 0 0 -pi/2 0]);
L(5)=Link([0 6 0 0 0]); 

%Se monta el Robot
RTrabajo = SerialLink(L);
RTrabajo.name = 'Manita';

%Limite al plot 3D
RTrabajo.plot([0 0 0 0 0],'workspace',[-100 200 -100 200 -100 200]);
RTrabajo.teach();

%DIMENSIONES ROBOT
l1=30;
l2=34.5;
l3=17;
l4=6;
ly=5;
%%%%%%%%%%%%valores prueba-> ENTRADAS%%%%%%%%%%%%%
pitch=pi/2;  %pi/2 es para rriba
roll=0;
xp=-8.6;
yp=24.8;
zp=60.14;

%%%%%%%%%%CINEMÁTICA INVERSA%%%%%%%%

%%%  Q1 %%
Rp=sqrt(yp^2+xp^2-ly^2);

theta1=atan2(yp,xp)-atan2(ly,Rp);

%%% MUÑECA %%
z=zp-sin(pitch)*l4;
y=yp-sin(theta1)*cos(pitch)*l4;
x=xp-cos(theta1)*cos(pitch)*l4;

%%% RESTO %%
R=sqrt(y^2+x^2-ly^2);
cost3=(R^2+(z-l1)^2-l2^2-l3^2)/(2*l2*l3);

theta3=atan2(-sqrt(1-cost3^2),cost3);
theta2=atan2((z-l1),R)-atan2((l3*sin(theta3)),(l2+l3*cost3));
theta4=pitch-theta3-theta2;
theta5=roll;
thetas={(theta1)*180/pi (-theta2)*180/pi -theta3*180/pi -theta4*180/pi-90 theta5*180/pi} %%%%%%% REVISAR


%%% DIBUJO %%%
RTrabajo.fkine([theta1,-theta2,-theta3,-theta4-pi/2,theta5])
RTrabajo.plot([theta1,-theta2,-theta3,-theta4-pi/2,theta5])
