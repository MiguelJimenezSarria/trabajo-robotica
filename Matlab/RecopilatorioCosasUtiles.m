
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RECOPILATORIO COSAS MARCO%%%%%%%
% Cinematica inversa
%La inversa de la matriz de transformaci�n homogenea, funcion abajo

%para q1
Q1matriz1=invMatriz(A01)*MatrizDirecta
Q1matriz2=A12*A23*A34*A45

Q1matriz2(4,1) %=0
Q1matriz2(4,2) %=0
Q1matriz2(3,3) %=0
Q1matriz2(4,3) %=0
Q1matriz2(3,4) %= 9
Q1matriz2(4,4) % =1

Q1matriz1(4,1) %=0
Q1matriz1(4,2) %=0
Q1matriz1(3,3) % no 0, no usar porque depende del resto de qs
Q1matriz1(4,3) %=0
Q1matriz1(3,4) %= no 0, no usar porque depende del resto de qs
Q1matriz1(4,4) 

%para q2
Q2matriz=invMatriz(A12)*invMatriz(A01)*MatrizDirecta;
A23*A34*A45;
%igualarlas y despejar q2

%para q3
Q3matriz=invMatriz(A23)*invMatriz(A12)*invMatriz(A01)*MatrizDirecta;
A34*A45;
%igualarlas y despejar q3

%para q4
Q4matriz=invMatriz(A34)*invMatriz(A23)*invMatriz(A12)*invMatriz(A01)*MatrizDirecta;
A45;

%para q5
Q5matriz=invMatriz(A45)*invMatriz(A34)*invMatriz(A23)*invMatriz(A12)*invMatriz(A01)*MatrizDirecta;
%I;



%%%Cinematica diferencial
syms q1 q2 q3 q4 q5 dq1 dq2 dq3 dq4 dq5 real  
Toriginal=RTrabajo.fkine([q1,q2,q3,q4]);

dR=diff(Roriginal,q1)*dq1+diff(Roriginal,q2)*dq2+diff(Roriginal,q3)*dq3+diff(Roriginal,q4)*dq4
omega=simplify(dR*Roriginal')
w=[omega(3,2);omega(1,3);omega(2,1)]
Jo=[diff(w,dq1) diff(w,dq2) diff(w,dq3) diff(w,dq4)]


 J=RTrabajo.jacob0([q1, q2, q3, q4,q5]);
function Jacobiana_geometrica()

    Toriginal=RTrabajo.fkine([q1,q2,q3,q4]);
    x_punto=diff(Toriginal.t(1,1),q1)+diff(Toriginal.t(1,1),q2)+diff(Toriginal.t(1,1),q3)++diff(Toriginal.t(1,1),q4)
    y_punto=diff(Toriginal.t(1,2),q1)+diff(Toriginal.t(1,2),q2)+diff(Toriginal.t(1,2),q3)++diff(Toriginal.t(1,2),q4)
    z_punto=diff(Toriginal.t(1,3),q1)+diff(Toriginal.t(1,3),q2)+diff(Toriginal.t(1,3),q3)++diff(Toriginal.t(1,3),q4)
end
function M = invMatriz(m)
%Separamos las matrices dentro de la matriz de transformaci�n homogenea
R=m(1:3,1:3);  
P=m(1:3,4);

% La inversa de la matriz de rotacion "interna" es su transpuesta
Rinv=transpose(R);

M(1:3,1:3)=Rinv;
M(1:3,4)=-Rinv*P;
M(4,1:4)={0,0,0,1};
end

function trayectoria()
    pos1=[RTrabajo.fkine([0 0 0 0]).t(1:3)];  %empieza
    pos2=[RTrabajo.fkine([pi/2 -pi/2 pi/2 pi/2]).t(1:3)]; %acaba
    q=[0;0;0;0];
    pasos=100;
    v=(pos2-pos1)/pasos; %velocidad
    RTrabajo.plot(q');
    RTrabajo.teach();
    for i=0:1/pasos:1
        obj=pos2*i+pos1*(1-i);
        dp=obj-pos;
        Jt=RTrabajo.jacob0(q);
        Jp=Jt(1:3,1:3)
        dq=inv(Jp)*dp
        q=q+dq
        
        pos=[RTrabajo.fkine(q').t(1:3)]
    end
end

%%DE LA INVERSA
%paso de fkni
MatrizDirecta(1:3,1)=RTrabajo.fkine([v1,v2,v3,v4,v5]).n;
MatrizDirecta(1:3,2)=RTrabajo.fkine([v1,v2,v3,v4,v5]).o;
MatrizDirecta(1:3,3)=RTrabajo.fkine([v1,v2,v3,v4,v5]).a;
MatrizDirecta(1:3,4)=RTrabajo.fkine([v1,v2,v3,v4,v5]).t;
MatrizDirecta(4,1:4)=[0,0,0,1];


T=transl(2,4,-1)*rpy2tr(0,90,180, 'deg');
q = RTrabajo.ikine(T,[0 0 0 0 0],[1 1 1 0 0 0])

function movimiento(x,y,z,theta1,theta2,theta3)
T=transl(x,y,z)*rpy2tr(theta1,theta2,theta3, 'deg');
q=RTrabajo.ikine(T) % (T=a donde quiero ir, [000] donde empiezo, [1 001] que grados busco

end



%ejemplos de movimiento

function q1=Inversa(q,x,y,z,theta1,theta2,theta3)
T=transl(x,y,z)*rpy2tr(theta1,theta2,theta3, 'deg');
%T=SE3(x,y,z)*SE3.Rx(theta1)*SE3.Ry(theta2)*SE3.Rz(theta3);
q1=RTrabajo.ikine(T,q,[1 1 1 0 0 0]) % (T=a donde quiero ir, [000] donde empiezo, [1 001] que grados busco
end
function movimiento(q0,q1)  %el q1 se saca con la funci�n anterior
t=[0:0.005:2]';
q=RTrabajo.jtraj(q0,q1,t);
RTrabajo.plot(q)  %simula el movimiento
qplot(t,q)   %dibuja trayectorias
end

function movimiento(q0,q1)  %el q1 se saca con la funci�n anterior
t=[0:0.005:2]';
q=RTrabajo.jtraj(q0,q1,t);
RTrabajo.plot(q)  %simula el movimiento
qplot(t,q)   %dibuja trayectorias
end

%%%%%%%%PRUEBA IKINE
%%Cinematica directa
clear all, clc

%Parametros DH

% Tita, d, a, alfa, TIPO
L(1)=Link([0 30 0 -pi/2 0]);
L(2)=Link([-pi/2 2 34.5 0 0]); 
L(3)=Link([0 7 17 0 0]);
L(4)=Link([-pi/2 0 0 -pi/2 0]);
L(5)=Link([0 0 0 0 0]); 

%Se monta el Robot
RTrabajo = SerialLink(L);

RTrabajo.name = 'Manita';



%Limite al plot 3D
%RTrabajo.plot([0 0 0 0],'workspace',[-100 200 -100 200 -100 200]);
% condiciones iniciales de las q    limites del espacio

RTrabajo.teach();
%habilitar la paleta para moverlo

q=[1.69 2.75 -1.04 1.43 -1.57];
q1=[0 2 0 3 0];
p=RTrabajo.fkine(q) %Calcula la posicion

q2=RTrabajo.ikine(p, q1, [1 1 1 0 0 0]);

RTrabajo.plot(q2)




%%trayectorias
pos1=[RTrabajo.fkine([0 0 0 0 0]).t(1:3)];  %empieza
pos2=[RTrabajo.fkine([pi/2 -pi/2 pi/2 pi/2 pi/2]).t(1:3)]; %acaba
q=[0;0;0;0:0]; %plot(qz)
pasos=100;
v=(pos2-pos1)/pasos; %velocidad
RTrabajo.plot(q');
RTrabajo.teach();
for i=0:1/pasos:1
  obj=pos2*i+pos1*(1-i);
  dp=obj-pos;
  Jt=RTrabajo.jacob0(q);
  Jp=Jt(1:3,1:3)
  q=inv(Jp)*dp
  q=q+dq
  pos=[RTrabajo.fkine(q').t(1:3)]
end 

%%TRAYECTORIAS OTRO
t=0:0.005:0.2;
p1=[-1.39 0 0 0 0];
p2=[1.39 0 0 0 0];
q=jtraj(p1,p2,t);

for c = 1:40 % al tama�o de q
     T=RTrabajo.fkine(q(c,:)).t;
  hold on;
   plotp( T(1:3,1));
end

  
   
   RTrabajo.plot(q)  %simula el movimiento
  




%qplot(t,q)   %dibuja trayectorias