%%Cinematica directa
clear all, clc

%Parametros DH

% Tita, d, a, alfa, TIPO
L(1)=Link([-pi 30 0 -pi/2 0]);
L(2)=Link([-pi/2 -2 34.5 0 0]); 
L(3)=Link([0 7 17 0 0]);
L(4)=Link([-pi/2 0 0 -pi/2 0]);
L(5)=Link([0 6 0 0 0]); 

%Se monta el Robot
RTrabajo = SerialLink(L);

RTrabajo.name = 'Manita';

%Limite al plot 3D
RTrabajo.plot([0 -pi/2 0 -pi/2 0],'workspace',[-100 200 -100 200 -100 200]);
% condiciones iniciales de las q    limites del espacio

RTrabajo.teach();

syms v1 v2 v3 v4 v5
%%% PARA SIMPLIFICAR EL PROCESO SE DEJA LA CONVERSION DE GRADOS A RADIANES
%%% (ESTAMOS LEYENDO EN GRADOS EN ARDUINO Y AQUI TRABAJAMOS CON RADIANES)
A01=trchain('Rz(v1-pi)Tz(30)Rx(-pi/2)');
A12=trchain('Rz(v2-pi/2)Tz(-2)Tx(34.5)');
A23=trchain('Rz(v3)Tz(7)Tx(17)');
A34=trchain('Rz(v4-pi/2)Rx(-pi/2)');
A45=trchain('Rz(v5)Tz(6)');


AT=A01*A12*A23*A34*A45
P=AT(1:3,4);
Px=P(1);
Py=P(2);
Pz=P(3);
T=RTrabajo.fkine([v1-pi,v2-pi/2,v3,v4-pi/2,v5])


%%%%%%%%%%%%%%%%%%JACOBIANA DE POSICION%%%%%%%%%%%%%%%

Jpos=[diff(P,v1) diff(P,v2) diff(P,v3) diff(P,v4) diff(P,v5)]

%%%%%%%%%%JACOBIANA DE ORIENTACION%%%%%%%%%%%%%%%
syms  dq1 dq2 dq3 dq4 dq5 
Roriginal=AT(1:3,1:3);
Rt=transpose(Roriginal);
dR=diff(Roriginal,v1)*dq1+diff(Roriginal,v2)*dq2+diff(Roriginal,v3)*dq3+diff(Roriginal,v4)*dq4+diff(Roriginal,v5)*dq5;
omega=dR*Rt;
w=[omega(3,2);omega(1,3);omega(2,1)];
Jori=[diff(w,dq1) diff(w,dq2) diff(w,dq3) diff(w,dq4) diff(w,dq5)]

J(1:3,1:5)=Jpos;
J(4:6,1:5)=Jori;
J
Jpetercorke=RTrabajo.jacob0([v1-pi, v2-pi/2, v3, v4-pi/2,v5])
Js=Jpetercorke(1:5,1:5);


